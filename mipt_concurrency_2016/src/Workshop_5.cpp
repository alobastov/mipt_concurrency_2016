﻿#include <Common.h>

#include <Workshop_5.h>
#include <timeit.h>

using namespace std;

class a {
public:
};
struct b {};
class aa : a {};
struct bb : b {};

// группа 9:00
template<typename T>
class FineGrainedQueue {
private:

	struct Node {
		unique_ptr<Node> Next;
		shared_ptr<T> Data;
	};

	unique_ptr<Node> head;
	Node* tail;
	mutex headMutex;
	mutex tailMutex;
	condition_variable signal;

	Node* getTail()
	{
		lock_guard<mutex> lock(tailMutex);
		return tail;
	}

public:
	FineGrainedQueue() : head(new Node), tail(head.get())
	{}

	void Push(T data)
	{
		shared_ptr<T> newData = make_shared<T>(move(data));
		unique_ptr<Node> newNode(new Node);
		Node* newTail = newNode.get();

		lock_guard<mutex> lock(tailMutex);
		tail->Data = newData;
		tail->Next = move(newNode);
		
		tail = newTail;

		signal.notify_one();
	}

	shared_ptr<T> WaitAndPop()
	{
		unique_lock<mutex> lock(headMutex);

		signal.wait(lock, [this] () {return head.get() != getTail(); });

		unique_ptr<Node> oldHead = move(head);
		head = move(oldHead->Next);

		return oldHead->Data;
	}

};

// группа 10:30
// Потокобезопасная очередь, блокирующая отдельно голову и хвост.
template<typename T>
class SmartQueue {
private:
	
	struct Node {
		shared_ptr<T> Data;
		unique_ptr<Node> Next;
	};

	mutex headMutex;
	mutex tailMutex;
	condition_variable signal;
	unique_ptr<Node> head;
	Node* tail;
	Node* getTail()
	{
		lock_guard<mutex> lock(tailMutex);
		return tail;
	}

public:

	SmartQueue() : head(new Node), tail(head.get())
	{}

	void Push(T data)
	{
		shared_ptr<T> newData = make_shared<T>(move(data));
		unique_ptr<Node> newNode(new Node);
		Node* newTail = newNode.get();

		lock_guard<mutex> lock(tailMutex);
		tail->Data = newData;
		tail->Next = move(newNode);
		tail = newTail;
		signal.notify_one();
	}

	shared_ptr<T> WaitAndPop()
	{
		unique_lock<mutex> lock(headMutex);
		signal.wait(lock, [this] () {return head.get() != getTail(); });

		unique_ptr<Node> oldHead = move(head);
		head = move(oldHead->Next);
		return oldHead->Data;
	}
};
/*
static int partition(vector<int>& a, int left, int right)
{
	int leftEnd = left;
	int rightEnd = right;

	int pivot = a[right];
	for( ;left < right; ) {
		if (a[left] > pivot || a[right] <= pivot) {
			swap(a[left], a[right]);
			left++; right--;
		}

		if (a[left] <= pivot) left++;
		if (a[right] > pivot) right--;
	}
	swap(a[rightEnd], a[left]);
	return left;
}
static void PMQSort(vector<int>& a, size_t left, size_t right)
{
	if(right - left <= 1) {
		return;
	}

	int newLeft = partition(a, left, right);

	PMQSort(a, left, newLeft);
	PMQSort(a, newLeft, right);
}
void PQSort(vector<int>& a)
{
	if(a.size() <= 1) {
		return;
	}

	PMQSort(a, 0, a.size() - 1);
}
*/
void printArray(const vector<int>& a)
{
	for_each(a.begin(), a.end(), [](int i) {
		cout << i << " ";
	});
	
	cout << endl;
}

static void workshop_6()
{
	cout << "====================\nWorkshop 6\n====================\n\n";

	vector<int> a = { 1, 2, 5, 4, 5};

	//PQSort(a);

	printArray(a);



	//std::this_thread::sleep_for(std::chrono::milliseconds(5500));
}

//----------------------------

int so_partition(vector<int>& arr, int left, int right)
{
	int pivot = arr[left];
	while (left != right)
	{
		if (arr[left] > arr[right])
		{
			swap(arr[left], arr[right]);
		}
		if (pivot == arr[left])
			right--;
		else // Pivot == arr[right]
			left++;
	}
	return left;
}

static int partition(vector<int>& toPart, int from, int to)
{
	int pivot = toPart[from]; //pivot =  шкворень
	int j = from;
	int k = from;

	for (int i = from; i < to; i++) {
		if (toPart[i] < pivot) {
			swap(toPart[k], toPart[j]);
			j++;
		}
		k++;
	}

	swap(pivot, toPart[j]);
	return j;
}

static void parallelQSortInternal(vector<int>& toSort, int from, int to)
{
	if (to - from <= 1) {
		return;
	}

	int pivot = partition(toSort, from, to);

	if (to - from < 100) {
		parallelQSortInternal(toSort, from, pivot);
		parallelQSortInternal(toSort, pivot + 1, to);
	}
	else {
		std::future<void> fut =
			std::async(std::launch::async, [&]() {
			parallelQSortInternal(toSort, from, pivot);
		});

		parallelQSortInternal(toSort, pivot + 1, to);
		fut.get();
	}
}

void ParallelQSort(vector<int>& toSort)
{
	timeit timer;
	int from = 0;
	int to = toSort.size();
	parallelQSortInternal(toSort, from, to);
}

static void workshop_6_10_30()
{
	cout << "====================\nWorkshop 6 10:30\n====================\n\n";

	vector<int> toSort = vector<int>(1000, 0);
	//partition(toSort, 0, toSort.size());

	ParallelQSort(toSort);

	printArray(toSort);
}

void CWorkshop_5::Run()
{
	workshop_6_10_30();
	return;
	cout << "====================\nWorkshop 5\n====================\n\n";
	static const int threadNumber = 100;
	SmartQueue<int> q;
	thread([&q] () {
			for(int i = 0; i < threadNumber; i++) {
				q.Push(i);
			}
		}).detach();

	thread([&q] () {
		for(int i = 0; i < threadNumber; i++) {
			cout << *q.WaitAndPop() << endl;
		}
	}).detach();

	std::this_thread::sleep_for(std::chrono::milliseconds(5500));
}

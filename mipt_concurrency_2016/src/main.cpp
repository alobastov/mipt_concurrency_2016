//
//  main.cpp
//  mipt_concurrency_2016
//
//  Created by Lobastov Alexander on 19/02/16.
//  Copyright © 2016 axorb. All rights reserved.
//

#include <Common.h>

#include <Workshop_4.h>
#include <Workshop_5.h>
#include <timeit.h>

using namespace std;

class spinlock {
public:
	spinlock() : locked(false)
	{
	}

	void lock()
	{
		while(locked.exchange(true)) {
			// wait
		}
	}

	void unlock()
	{
		locked.store(false);
	}

private:
	std::atomic_bool locked;
};

class ticket_spinlock {
private:
	using ticket = std::size_t;

public:
	ticket_spinlock() : owner_ticket(0), next_free_ticket(0)
	{
	}

	void lock()
	{
		ticket this_thread_ticket = next_free_ticket.fetch_add(1);
		while(owner_ticket.load() != this_thread_ticket) {
			// wait
		}
	}

	void unlock()
	{
		owner_ticket.store(owner_ticket.load() + 1);
	}

private:
	std::atomic<ticket> owner_ticket;
	std::atomic<ticket> next_free_ticket;
};

static spinlock myMutex;
static mutex sysMutex;
static ticket_spinlock ticMutex;

static void Greeting()
{
	lock_guard<spinlock> tickGuard(myMutex);
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	cout << "Hello, World!" << endl;
}

// mutex, RAII
static void firstWorkshop()
{
	timeit timer;

	static const size_t threadNumber = 5;
	vector<thread> threads;
	for(size_t i = 0; i < threadNumber; i++) {
		threads.emplace_back(Greeting);
		threads.emplace_back([] () { Greeting(); });
	}

	for(thread &i : threads) {
		i.join();
	}
}

template<typename T>
static vector<T> functionWithParams()
{
	return vector<T>();
}

template<typename... Targs>
static vector<std::common_type_t<Targs...> > functionWithParams(Targs&&... Fargs)
{
	return vector<std::common_type_t<Targs...> > { forward<Targs>(Fargs)... };
}

// async
static void secondWorkshop()
{
	// packaged_task
	std::packaged_task<int()> task([] () { 
		std::this_thread::sleep_for(std::chrono::milliseconds(1000)); 
		//throw 1;
		return 7;
	});
	std::future<int> f1 = task.get_future();
	std::thread(std::move(task)).detach();

	try {
		//cout << f1.get() << endl;
	} catch(int& a) {
		cout << a << endl;
	}


	// async()
	std::future<int> f2 =
		std::async(std::launch::async, [] () { return 8; });

	// promise
	std::promise<int> p;
	std::shared_future<int> f3 = p.get_future();
	auto t = std::thread([] (std::promise<int> p) {

		p.set_value(9);
	
	}, std::move(p));
	t.detach();

	std::cout << "Waiting..." << std::flush;
	std::cout << "Done!\nResults are: " << f1.get()
		<< ' ' << f2.get() << ' ' << f3.get() << f3.get() << '\n';

	auto v = functionWithParams("string", "string");
}

void f(int n1, int n2, int n3, const int& n4, int n5)
{
	std::cout << n1 << ' ' << n2 << ' ' << n3 << ' ' << n4 << ' ' << n5 << '\n';
}
int g(int n1)
{
	return n1;
}
struct Foo {
	void print_sum(int n1, int n2)
	{
		std::cout << n1 + n2 << '\n';
	}
	int data = 10;
};

void bindSamples()
{
	using namespace std::placeholders;  // for _1, _2, _3...
	int n = 7;
	auto f1 = std::bind(f, _2, _1, 42, std::cref(n), n);
	n = 10;
	f1(1, 2, 1001); 

	auto f2 = std::bind(f, _3, std::bind(g, _3), _3, 4, 5);
	f2(10, 11, 12);

	std::default_random_engine e;
	std::uniform_int_distribution<> d(0, 10);
	std::function<int()> rnd = std::bind(d, e); 
	for(int n = 0; n < 10; ++n) {
		std::cout << d(e) << ' ';
		std::cout << rnd() << ' ';
	}
	std::cout << '\n';

	Foo foo;
	auto f3 = std::bind(&Foo::print_sum, &foo, 95, _1);
	f3(5);

	auto f4 = std::bind(&Foo::data, _1);
	std::cout << f4(foo) << '\n';

	std::cout << f4(std::make_shared<Foo>(foo)) << '\n'
		<< f4(std::unique_ptr<Foo>(new Foo(foo))) << '\n';
}

void functionSamples()
{
	std::function<void()> f_display_42 = [] () { cout << g(42) << endl; };
	f_display_42();

	Greeting();
	std::function<void()> fGreeting = Greeting;
	fGreeting();

	void(__cdecl *a)(void) = Greeting;
	a();

	void(*greetingPointer)() = Greeting;
	greetingPointer();
}

void f()
{
	lock_guard<mutex> lock(sysMutex);



	{
		unique_lock<mutex> ll(sysMutex, std::defer_lock);
	}
}

//---------------------------

static queue<int> GlobalQueue;
static mutex GlobalQueueMutex;
static condition_variable GlobalQueueSignal;
static const int ThreadNumber = 10;
static int WorkedThreadCount = 0;
static condition_variable AllWorkDoneSignal;

// conditional variables
static void thirdWorkshop()
{
	for(int i = 0; i < ThreadNumber; i++) {
		thread consumer([] () {
			unique_lock<mutex> lock(GlobalQueueMutex);
			GlobalQueueSignal.wait(lock, [] () {return !GlobalQueue.empty(); });
			cout << GlobalQueue.front() << endl;
			GlobalQueue.pop();

			WorkedThreadCount++;
			if(WorkedThreadCount == ThreadNumber) {
				AllWorkDoneSignal.notify_one();
			}
		});

		consumer.detach();
	}

	for(int i = 0; i < ThreadNumber; i++) {
		thread producer([i] () {
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			lock_guard<mutex> lock(GlobalQueueMutex);
			GlobalQueue.push(i);
			GlobalQueueSignal.notify_one();
		});

		producer.detach();
	}

	unique_lock<mutex> lock(GlobalQueueMutex);
	AllWorkDoneSignal.wait(lock);

	/// ---

	bindSamples();
	functionSamples();

	// auto v = functionWithParams(1, 2, 3UL, 4LL);
	// auto vv = functionWithParams<char*>();
}


class myException : public std::runtime_error {
public:
	myException(const char* message) : runtime_error(message)
	{}



};

int main(int argc, const char *argv[])
{
	// firstWorkshop();
	// secondWorkshop();

	// thirdWorkshop();

	CWorkshop_4 workshop_4;
	CWorkshop_5 workshop_5;

	IWorkshop& current = workshop_5;

	try {
		throw myException("error message");
	}
	catch (std::exception& exc) {
		std::cout << exc.what() << std::endl;
	}
	catch (...) {
	
	}

	current.Run();

	return 0;
}

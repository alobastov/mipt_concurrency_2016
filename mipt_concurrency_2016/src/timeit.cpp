#include <Common.h>

#include <timeit.h>

timeit::timeit() : start(clock())
{
}

timeit::~timeit()
{
	std::cout << (clock() - start) << std::endl;
}

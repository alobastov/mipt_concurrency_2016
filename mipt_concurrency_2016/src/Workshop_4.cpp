﻿#include <Common.h>

#include <Workshop_4.h>

using namespace std;

static const int ThreadNumber = 20;

// группа 9:00 часов
class CSharedMutex {
public:
	CSharedMutex() : lockGuard(internalMutex, defer_lock_t())
	{}

	// для писателей
	void lock()
	{
		lockGuard.lock();
		signalReaderOut.wait(lockGuard, [this] () {return readersCount.load() == 0; });
		writing.store(true);
	}

	void unlock()
	{
		lockGuard.unlock();
		writing.store(false);
		signalWriterOut.notify_all();
	}

	// для читателей
	void lock_shared()
	{
		while(writing.load()) {}
		readersCount.fetch_add(1);
	}

	void unlock_shared()
	{
		readersCount.fetch_sub(1);
		signalReaderOut.notify_all();
	}

private:
	unique_lock<mutex> lockGuard;
	condition_variable signalReaderOut;
	condition_variable signalWriterOut;
	mutex internalMutex;
	atomic_bool writing;
	atomic<int> readersCount;
};

// 10:30
class MySharedMutex {
public:
	MySharedMutex() : writing(false), readersCount(0) {}
	void lock()
	{
		while(readersCount.load() != 0 || writing.load()) {
			this_thread::yield();
		}
		internalMutex.lock();
		writing.store(true);
	}
	void unlock()
	{
		writing.store(false);
		internalMutex.unlock();
	}
	void lock_shared()
	{
		while(writing.load()) {
			this_thread::yield();
		}
		readersCount.fetch_add(1);
	}
	void unlock_shared()
	{
		readersCount.fetch_sub(1);
	}
private:
	mutex internalMutex;
	atomic_bool writing;
	atomic_int readersCount;
};

static MySharedMutex myMutex;


static void Greeting()
{
	//lock_guard<MySharedMutex> tickGuard(myMutex);
	shared_lock<MySharedMutex> tickGuard(myMutex);
	
	cout << "Hello, World!" << endl;
}

void CWorkshop_4::Run()
{
	cout  << "====================\nWorkshop 4\n====================\n\n";


	
	for(int i = 0; i < ThreadNumber; i++) {
		cout << i;
	}
	cout << endl;

	for(int i = 0; i < ThreadNumber; i++) {
		thread t([i] () {
			Greeting();
		});

		t.detach();
	}

	for(int i = 0; i < ThreadNumber; i++) {
		thread t([i] () {
			Greeting();
		});

		t.detach();
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(500));
}

﻿// 
// Автор: Александр Лобастов
// Описание: Класс для работы на 5 семинаре.

#pragma once

#include <Workshop.h>

// Класс для демонстрации и написания кода на 5 семинаре.
class CWorkshop_5 : public IWorkshop {
public:
	virtual void Run();
};

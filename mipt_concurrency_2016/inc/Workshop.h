﻿#pragma once

// Класс - шаблон для семинаров.
struct IWorkshop {
public:
	// Деструктор тоже пусть будет виртуальным.
	virtual ~IWorkshop() = default;

	// Выполнить код с семинара.
	virtual void Run() = 0;
};
